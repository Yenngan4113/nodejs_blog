const newsRouter = require("./news");
const siteRouter = require("./site");

function routes(app) {
  app.use("/news", newsRouter);
  app.use("/", siteRouter);

  app.get("/", (req, res) => {
    res.send(`<h1>Hello World</h1>`);
  });
}

module.exports = routes;
