const express = require("express");
const { engine } = require("express-handlebars");
//path
const path = require("path");

//route
const routes = require("./routes");
//

// app là cả ứng dụng nodejs
const app = express();
// path

app.use(express.static(path.join(__dirname, "public")));
//http logger
const morgan = require("morgan");

const port = 3000;

app.use(morgan("combined"));

//Template Engine
app.engine(".hbs", engine({ extname: ".hbs" }));
app.set("view engine", ".hbs");
app.set("views", path.join(__dirname, "resources/views"));

// Routes init
routes(app);
//
//127.0.0.1 - localhost
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
